const path                    = require('path');
const webpack                 = require('webpack');
const ExtractTextPlugin       = require("extract-text-webpack-plugin");
const UglifyJSPlugin          = require('uglifyjs-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const BrowserSyncPlugin       = require('browser-sync-webpack-plugin');
const CleanWebpackPlugin      = require('clean-webpack-plugin');
const CopyWebpackPlugin       = require('copy-webpack-plugin');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const HtmlWebpackPlugin = require('html-webpack-plugin');

const VENDOR_LIBS = [
  'jquery', 'slick-carousel', '@fancyapps/fancybox'
];

const config = {
  entry: {
		bundle: './src/js/app.js',
    vendor: VENDOR_LIBS
	},
  output: {
      jsonpFunction:'webpackJsonp',
      path: path.join(__dirname, 'dist'),
      // IF YOU WANT TO PRODUCE JS'S WITHOUT CACHE MANAGEMENT
      filename: 'js/[name].js',
      // IF YOU WANT TO PRODUCE JS'S WITH CACHE MANAGEMENT
      // filename: 'js/[name].[chunkhash].js',
  },
	module: {
		rules: [
			{
				test: /\.scss$/,
				use: ExtractTextPlugin.extract({
					fallback: 'style-loader',
				  	use: ['css-loader', 'sass-loader', 'postcss-loader']
				}),
			},
			{
				test: /\.js|jsx$/,
				exclude: /(node_modules|bower_components)/,
				loader: 'babel-loader',
				query: {
					presets: ['react', 'es2015', 'stage-1']
				}
			}
		]
	},
  devServer: {
    historyApiFallback: true,
    contentBase: './'
  },
	plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'jQuery': 'jquery',
      'window.jQuery': 'jquery',
      'window.$': 'jquery'
    }),
    new webpack.optimize.CommonsChunkPlugin({
      names: ['vendor', 'manifest']
    }),
    // IF YOU WANT TO NOT PRODUCE INDEX.HTML FILE ON THE DIST FOLDER, COMMENT THE "HtmlWebpackPlugin" BELLOW
    new HtmlWebpackPlugin({
      template: 'src/index.html'
    }),
    new CleanWebpackPlugin(['dist']),
		new ExtractTextPlugin('/css/app.css'),
		new BrowserSyncPlugin({
        // WATCH ANY FILES
        files: [
            '**/*.*'
        ],
        // STATIC
		    // browse to http://localhost:3000/ during development,
        host: 'localhost',
		    port: 3000,
		    server: { baseDir: ['./dist'] },
		    files: [
		   	    '**/*.*'
		    ],
		    // when ./dist directory is being served
		    // host: 'localhost',
		    // port: 3000,
		    // server: { baseDir: ['public'] },
        // DEVELOPMENT SERVER
        // host: 'localhost',
        // port: 3000,
        // proxy: 'http://wpthemes.dev',
		}),
    new CopyWebpackPlugin([
            {from:'src/fonts',to:'fonts'},
            {from:'src/images',to:'images'}
    ]),
    new ImageminPlugin({
      disable: process.env.NODE_ENV !== 'production', // Disable during development
      pngquant: {
        quality: '90-100'
      },
      test: /\.(jpe?g|png|gif|svg)$/i
    }),
	],
};

//If true JS and CSS files will be minified
if (process.env.NODE_ENV === 'production') {
	config.plugins.push(
		new UglifyJSPlugin(),
		new OptimizeCssAssetsPlugin()
	);
}

module.exports = config;
