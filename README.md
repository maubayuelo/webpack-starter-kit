# WebpackPress Wordpress Theme

##Description.
It includes jQuery out of the box to show you how the compile works. It will create an bundle.min.js file (including into : Jquery, vendors, main files).

Just run ```sudo npm install``` and you should be all set.

Alternative run ```sudo npm install -g npm``` to run with updates.

##Commands:
1. ```npm run build``` - Will simply compile all your assets.
2. ```npm run watch``` - Will compile, start BrowserSync and watch for any changes.
3. ```npm run production``` - Will compile and minify all your assets. You may need to run ```npm install cross-env```.

##Note:
* On ```webpack.config.js```, on line ```server: { baseDir: [''] }``` on the quotes you must set the root f the folder where you want to load your html or php files
* Starter kit based on  [Webpack Tutorial ](https://www.youtube.com/watch?v=9kJVYpOqcVU) - Youtube tutorial
