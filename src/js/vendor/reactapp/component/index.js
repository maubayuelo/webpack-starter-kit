import React from 'react';
import ReactDOM from 'react-dom';
import {Component} from 'react';


// Create component. This Componet should produce some HTML
const App = function(){
  return <div>This is the react app</div>;
}
// Take the component and include it on the page with DOM
ReactDOM.render(<App />, document.querySelector('.app_container'));
