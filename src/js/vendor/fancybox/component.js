console.log('fancybox.js loaded');

import '@fancyapps/fancybox';

if ($("[data-fancybox]").length) // use this if you are using id to check
{
  $("[data-fancybox]").fancybox();
}
